from smartcard.CardType import CardType
from smartcard.CardRequest import CardRequest
from smartcard.util import toHexString, toBytes

class BankCardType(CardType):
	def matches( self, atr, reader=None ):
		if atr[0]==0x3B and atr[1]==0x65 and atr[2]==0x00 and atr[3]==0x00 :
			return True


# cb 3B 65 00 00 20 63 CB AD 80
cardtype = BankCardType()
cardrequest = CardRequest( timeout=4, cardType=cardtype )
cardservice = cardrequest.waitforcard()

cardservice.connection.connect()
print toHexString( cardservice.connection.getATR() )