var gui = require('nw.gui');
var fs = require('fs');

var credential = {};

function callbackAtrValue(value) {
  if(value != '' && value != undefined) {
    credential.atr = value;
    demoMode();
    //submitFormToApi(credential);
  } else {
    console.log('Card reading went wrong');
  }

}


function demoMode() {

  $('.access-granted').removeClass('hidden');
  $('.login').addClass('hidden');
  setTimeout(function(){

    $('.access-granted').addClass('hidden');
    $('.login').removeClass('hidden');
  }, 2000);


  fs.realpath('./app/utils/postRequest.html', {}, function (err, resolvedPath) {
    if (err) throw err;
    console.log(resolvedPath);
    var filePath = 'file://' + resolvedPath + '?app_token=yeomiGab&app_secret=memoireAuth2015#http://localhost:8080/auth';
    gui.Shell.openExternal(filePath);
  });
}


function submitFormToApi(credential) {
  $.ajax({
    url: "http://dev.serv-auth.com",
    method: 'POST',
    data: credential
  }).done(function(data) {
    var response = JSON.parse(data);
    $('.access-granted').removeClass('hidden');
    $('.login').addClass('hidden');

    fs.realpath('./app/utils/postRequest.html', {}, function (err, resolvedPath) {
      if (err) throw err;
      console.log(resolvedPath);
      var filePath = 'file://' + resolvedPath + '?key=' + response.key + '#http://dev.client-app.com';
      gui.Shell.openExternal(filePath);
    });
  });
}

$(document).ready(function() {
  $('.submit').click(function() {
    var e = $(this);
    credential.username = e.parent().find('.username').val();
    credential.password = e.parent().find('.password').val();
    readCardAtr(callbackAtrValue);

    return false;
  });
});