
var child = require('child_process');

function readCardAtr(callback) {
  var python = child.spawn( 'python', ['app.py']);
  var chunk = '';
  python.stdout.on('data', function(data){
    chunk += data;
  } );
  python.stdout.on('close', function( ){
    callback(chunk);
  } );
}